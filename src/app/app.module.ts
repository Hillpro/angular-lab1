import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routes } from './app.routes';
import { LoginComponent } from './components/login/login.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { RecipesComponent } from './components/recipes/recipes.component';
import { AuthService } from './services/auth.service';
import { NavComponent } from './components/nav/nav.component';
import { RecipeCreateComponent } from './components/recipe-create/recipe-create.component';
import { AuthGuardService } from './services/auth-guard.service';
import { RecipesService } from './services/recipes.service';
import { RecipeRowComponent } from './components/recipe-row/recipe-row.component';
import { RecipeViewComponent } from './components/recipe-view/recipe-view.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NotFoundComponent,
    RecipesComponent,
    NavComponent,
    RecipeCreateComponent,
    RecipeRowComponent,
    RecipeViewComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    AuthService,
    AuthGuardService,
    RecipesService,
  ],
  bootstrap: [
    AppComponent,
  ]
})
export class AppModule { }
