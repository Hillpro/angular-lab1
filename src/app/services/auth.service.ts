import { Injectable } from '@angular/core';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  private _currentUser: User = null;

  constructor() {
    this._currentUser = JSON.parse(localStorage.getItem('CurrentUser'));
  }

  get currentUser(): User {
    return this._currentUser;
  }

  login(email: string, password: string): User {
    if (email === 'a@a' && password === 'a') {
      this._currentUser = new User(1, email);
    } else if (email === 'b@b' && password === 'b') {
      this._currentUser = new User(2, email);
    } else if (email === 'c@c' && password === 'c') {
      this._currentUser = new User(3, email);
    }

    localStorage.setItem('CurrentUser', JSON.stringify(this._currentUser));

    return this._currentUser;
  }

  logout() {
    this._currentUser = null;
    localStorage.setItem('CurrentUser', null);
  }
}
