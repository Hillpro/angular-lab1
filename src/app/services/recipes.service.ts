import { Injectable } from '@angular/core';
import { Recipe } from '../models/recipe.model';
import { RecipeCategory } from '../models/recipe_category.model';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class RecipesService {

  private _recipes: Recipe[];
  private idIncrement: number;

  constructor(private authService: AuthService) {
    this._recipes = JSON.parse(localStorage.getItem('Recipes')) || [];
    this.idIncrement = Number(localStorage.getItem('AutoIncrement')) || 0;
  }

  get recipes(): Recipe[] {
    const userRecipes = this._recipes.filter(i => i.userId === this.authService.currentUser.id);
    return userRecipes;
  }

  get(id: number): Recipe {
    const userRecipes = this._recipes.filter(i => i.userId === this.authService.currentUser.id);
    return userRecipes.find(i => i.id === id);

    return this._recipes.find(i => i.id === id);
  }

  create(name: string, description: string, category: RecipeCategory) {
    this.idIncrement++;
    const newRecipe = new Recipe(this.idIncrement, this.authService.currentUser.id, name, description, category);
    this._recipes.unshift(newRecipe);

    localStorage.setItem('Recipes', JSON.stringify(this._recipes));
    localStorage.setItem('AutoIncrement', JSON.stringify(this.idIncrement));
  }

  update(id: number, name: string, description: string, category: RecipeCategory) {
    const updatedRecipe: Recipe = this.get(id);

    updatedRecipe.name = name;
    updatedRecipe.description = description;
    updatedRecipe.category = category;

    this._recipes = this._recipes.filter(i => i.id !== id);
    this._recipes.unshift(updatedRecipe);

    localStorage.setItem('Recipes', JSON.stringify(this._recipes));
  }

  delete(recipe: Recipe) {
    this._recipes = this._recipes.filter(i => i.id !== recipe.id);

    localStorage.setItem('Recipes', JSON.stringify(this._recipes));
  }
}
