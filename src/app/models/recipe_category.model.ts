export enum RecipeCategory {
  'Starter',
  'Main Course',
  'Dessert',
}
