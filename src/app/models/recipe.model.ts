import { RecipeCategory } from './recipe_category.model';

export class Recipe {
  id: number;
  userId: number;
  category: RecipeCategory;
  name: string;
  description: string;

  constructor(id: number, userId: number, name: string, description: string, category: RecipeCategory) {
    this.id = id;
    this.userId = userId;
    this.category = category;
    this.name = name;
    this.description = description;
  }
}
