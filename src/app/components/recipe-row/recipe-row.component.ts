import { Component, OnInit, Input } from '@angular/core';
import { Recipe } from 'src/app/models/recipe.model';
import { RecipesService } from 'src/app/services/recipes.service';
import { Router } from '@angular/router';

@Component({
  // tslint:disable-next-line: component-selector
  selector: '[app-recipe-row]',
  templateUrl: './recipe-row.component.html',
  styleUrls: ['./recipe-row.component.css']
})
export class RecipeRowComponent implements OnInit {

  @Input() recipe: Recipe;

  constructor(private recipesService: RecipesService, private router: Router) { }

  ngOnInit() {
  }

  delete() {
    this.recipesService.delete(this.recipe);
  }

  view() {
    this.router.navigate(['/recipes/', this.recipe.id]);
  }
}
