import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RecipesService } from 'src/app/services/recipes.service';
import { Recipe } from 'src/app/models/recipe.model';
import { FormGroup, FormControl, FormBuilder, Validators, NgForm } from '@angular/forms';

@Component({
  selector: 'app-recipe-view',
  templateUrl: './recipe-view.component.html',
  styleUrls: ['./recipe-view.component.css']
})
export class RecipeViewComponent implements OnInit {

  @ViewChild('form', {static: true}) formElement: NgForm;
  @ViewChild('submitButton', {static: true}) submitButton: ElementRef;

  recipe: Recipe;
  recipeForm: FormGroup;

  constructor(formBuilder: FormBuilder, route: ActivatedRoute, private recipesService: RecipesService, private router: Router) {
    route.paramMap.subscribe(params => {
      this.recipe = this.recipesService.get(Number(params.get('id')));

      if (this.recipe == null) {
        this.router.navigate(['/recipes']);
      }
    });

    // route.snapshot.paramMap.get('id');

    this.recipeForm = formBuilder.group(
      {
        name: new FormControl('', Validators.required),
        description: new FormControl('', Validators.required),
        category: new FormControl('', Validators.required),
      }
    );

    this.recipeForm.controls.name.setValue(this.recipe.name);
    this.recipeForm.controls.description.setValue(this.recipe.description);
    this.recipeForm.controls.category.setValue(this.recipe.category);
  }

  ngOnInit() {
  }

  update() {
    if (this.recipeForm.valid) {
      const recipesValues = this.recipeForm.value;
      this.recipesService.update(this.recipe.id, recipesValues.name, recipesValues.description, recipesValues.category);

      this.formElement.resetForm();
      this.recipeForm.controls.name.setValue(this.recipe.name);
      this.recipeForm.controls.description.setValue(this.recipe.description);
      this.recipeForm.controls.category.setValue(this.recipe.category);

      this.router.navigate(['/recipes']);
    }
  }
}
