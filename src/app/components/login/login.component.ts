import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    if (this.authService.currentUser !== null) {
      this.router.navigate(['/recipes']);
    }
  }

  login(email: string, password: string) {
    if (this.authService.login(email, password)) {
      this.router.navigate(['/recipes']);
    } else {
      alert(`Email ou mot de passe incorrect`);
    }
  }
}
